import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  bookForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private service: ServiceService, private router: Router) { }

  ngOnInit() {
    this.bookForm = this.formBuilder.group({
      author_name: ['', Validators.compose([Validators.required])],
      book_name: ['', Validators.compose([Validators.required])],
      isbn: ['', Validators.compose([Validators.required, Validators.pattern("^[0-9]*$")])],
    });
  }

  addBook(){
    const values = {
      author_name: this.bookForm.controls.author_name.value,
      book_name: this.bookForm.controls.book_name.value,
      isbn: this.bookForm.controls.isbn.value,
    };

    this.service.addBooks(values.author_name, values.book_name, values.isbn).subscribe(res => {
      // console.log(name.name);
        let id = res['_id'];
        this.router.navigate(['/listar']);
      }, (err) => {
        console.log(err);
      });

  }

}
