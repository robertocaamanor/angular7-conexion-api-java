import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import { ServiceService } from '../../Service/service.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  bookForm: FormGroup;
  id: string='';
  book_name:string='';
  author_name:string='';
  isbn:string='';
  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router,private service:ServiceService) { }

  ngOnInit() {
    this.loadBook();
    this.bookForm = this.formBuilder.group({
      author_name: ['', Validators.compose([Validators.required])],
      book_name: ['', Validators.compose([Validators.required])],
      isbn: ['', Validators.compose([Validators.required, Validators.pattern("^[0-9]*$")])],
    });
  }

  loadBook(){
    this.service.viewBook(this.route.snapshot.params['id']).subscribe((data: any) => {
      this.id = data.id;
      this.bookForm.setValue({
        book_name: data.book_name,
        author_name: data.author_name,
        isbn: data.isbn
      });
    });

  }
  editBook(){
    const values = {
      author_name: this.bookForm.controls.author_name.value,
      book_name: this.bookForm.controls.book_name.value,
      isbn: this.bookForm.controls.isbn.value,
    };

    this.service.editBook(this.id, values.author_name, values.book_name, values.isbn).subscribe(res => {
      // console.log(name.name);
        let id = res['_id'];
        this.router.navigate(['/listar']);
      }, (err) => {
        console.log(err);
      });
  }

}
