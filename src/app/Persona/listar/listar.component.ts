import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../Service/service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {
  books;
  searchText;
  constructor(private serviceService: ServiceService, private router: Router) { }

  ngOnInit() {
    this.getAllBooks();
  }

  getAllBooks(){
    this.serviceService.getBooks().subscribe((data) => {
      console.log(data);
      this.books = data;
    });

  }

  Nuevo(){
    this.router.navigate(["add"]);
  }

  deleteBook(id){
    this.serviceService.deleteBook(id).subscribe(res => {
      // console.log(name.name);
        this.getAllBooks();
      }, (err) => {
        console.log(err);
      });

  }

}
