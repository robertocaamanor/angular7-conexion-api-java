import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ejemplo-crud';

  constructor(private router:Router){}
  
  Listar(){
    this.router.navigate(["listar"]);
  }

  Nuevo(){
    this.router.navigate(["add"]);
  }

  Music(){
    this.router.navigate(["music"]);
  }

  NewMusic(){
    this.router.navigate(["music/add"]);
  }
}
