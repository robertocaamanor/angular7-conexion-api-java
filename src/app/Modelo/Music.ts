export class Music{
    id: number;
    artist_name: String;
    album_name: String;
    genre: String;
    label: String;
}