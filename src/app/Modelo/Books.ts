export class Books{
    id: number;
    book_name: String;
    author_name: String;
    isbn: String;
}