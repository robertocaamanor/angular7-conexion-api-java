import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Books } from '../Modelo/Books';
import { Music } from '../Modelo/Music';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  // books: Books;
  constructor(private http: HttpClient) { }

  url = 'http://localhost:8080';

  getBooks() {
    return this
            .http
            .get<Books[]>(`${this.url}/books`);
  }

  addBooks(author_name, book_name, isbn){
    return this
            .http.post<Books[]>(`${this.url}/books`, { author_name: author_name, book_name: book_name, isbn: isbn });

  }

  viewBook(id_book){
    return this
            .http.get<Books[]>(`${this.url}/books/` + id_book);
  }

  editBook(id_book, author_name, book_name, isbn){
    return this
            .http
            .put(`${this.url}/books/` + id_book, { author_name: author_name, book_name: book_name, isbn: isbn });

  }

  deleteBook(id_book){
    return this
            .http
            .delete(`${this.url}/books/` + id_book);

  }

  getMusic() {
    return this
            .http
            .get<Music[]>(`${this.url}/music`);
  }

  addMusic(album_name, artist_name, genre, label, prize, stock, file){
    return this
            .http.post<Music[]>(`${this.url}/music`, { album_name: album_name, artist_name: artist_name, genre: genre, label:label, prize: prize, stock: stock, file: file });

  }

  viewMusic(id_music){
    return this
            .http.get<Music[]>(`${this.url}/music/` + id_music);
  }

  editMusic(id_music, album_name, artist_name, genre, label, prize, stock){
    return this
            .http
            .put(`${this.url}/music/` + id_music, { album_name: album_name, artist_name: artist_name, genre: genre, label:label, prize: prize, stock: stock });

  }

  deleteMusic(id_music){
    return this
            .http
            .delete(`${this.url}/music/` + id_music);

  }

  getHelloWorld(){
    return this
      .http
      .get(`https://www.mocky.io/v2/5185415ba171ea3a00704eed`);
  }

  thumbnailFetchUrl : string = "https://south/generateThumbnail?width=100&height=100";
  getBlobThumbnail(): Observable<Blob> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
    return this.http.post<Blob>(this.thumbnailFetchUrl,
      {
        "url": "http://acs/Logo.png"
      }, {headers: headers, responseType: 'blob' as 'json' });
  }

}
