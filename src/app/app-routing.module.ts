import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarComponent } from './Persona/listar/listar.component';
import { AddComponent } from './Persona/add/add.component';
import { EditComponent } from './Persona/edit/edit.component';
import { MusicListarComponent } from './Music/music-listar/music-listar.component';
import { MusicAddComponent } from './Music/music-add/music-add.component';
import { MusicEditComponent } from './Music/music-edit/music-edit.component';
import {HelloWorldComponent} from "./Hello/hello-world/hello-world.component";


const routes: Routes = [
  {path: 'listar', component: ListarComponent},
  {path: 'add', component: AddComponent},
  {path: 'edit/:id', component: EditComponent},
  {path: 'music', component: MusicListarComponent},
  {path: 'music/add', component: MusicAddComponent},
  {path: 'music/edit/:id', component: MusicEditComponent},
  {path: 'hello', component: HelloWorldComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
