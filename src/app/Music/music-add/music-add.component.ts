import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/Service/service.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {FileUploader} from 'ng2-file-upload';

@Component({
  selector: 'app-music-add',
  templateUrl: './music-add.component.html',
  styleUrls: ['./music-add.component.css']
})
export class MusicAddComponent implements OnInit {

  musicForm: FormGroup;
  submitted = false;
  selectedFile: File;
  uploader: FileUploader;
  url = 'http://localhost:8080';
  constructor(private service: ServiceService, private router: Router, private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.musicForm = this.formBuilder.group({
      album_name: ['', Validators.compose([Validators.required])],
      artist_name: ['', Validators.compose([Validators.required])],
      genre: ['', Validators.compose([Validators.required])],
      label: ['', Validators.compose([Validators.required])],
      prize: ['', Validators.compose([Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(4)])],
      stock: ['', Validators.compose([Validators.required, Validators.pattern("^[0-9]*$")])],
    });
  }

  onFileChanged(event){
    this.selectedFile = event.target.files[0];
    console.log(this.selectedFile);
  }

  addMusic(){
    this.submitted = true;
    if (this.musicForm.invalid) {
      return;
    }
    const values = {
       album_name: this.musicForm.controls.album_name.value,
       artist_name: this.musicForm.controls.artist_name.value,
       genre: this.musicForm.controls.genre.value,
       label: this.musicForm.controls.label.value,
       prize: this.musicForm.controls.prize.value,
       stock: this.musicForm.controls.stock.value,
     };

       this.service.addMusic(values.album_name, values.artist_name, values.genre, values.label, values.prize, values.stock, this.selectedFile.name).subscribe(res => {
         // console.log(name.name);
         let id = res['id'];
         this.router.navigate(['/music']);
         const formData = new FormData();
         formData.append('file', this.selectedFile);
         console.log(formData);
         this.http.post(`${this.url}/files?file=` + this.selectedFile.name, formData)
           .subscribe(res => {
             console.log(res);
           })
       }, (err) => {
         console.log(err);
       });

  }




}
