import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/Service/service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-music-listar',
  templateUrl: './music-listar.component.html',
  styleUrls: ['./music-listar.component.css']
})
export class MusicListarComponent implements OnInit {

  music;
  searchText;
  message = false;
  msgText = '';
  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit() {
    this.getMusic();
  }

  getMusic(){
    this.service.getMusic().subscribe((data) => {
      console.log(data);
      this.music = data;
    });
  }

  NewMusic(){
    this.router.navigate(["music/add"]);
  }

  deleteMusic(id){
    this.service.deleteMusic(id).subscribe(res => {
      // console.log(name.name);
        this.getMusic();
        this.message = true;
        this.msgText = 'Disco eliminado';
      }, (err) => {
        console.log(err);

      });

  }

}
