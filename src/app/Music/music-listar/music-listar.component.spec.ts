import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicListarComponent } from './music-listar.component';

describe('MusicListarComponent', () => {
  let component: MusicListarComponent;
  let fixture: ComponentFixture<MusicListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
