import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/Service/service.service';
import { Router, ActivatedRoute } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-music-edit',
  templateUrl: './music-edit.component.html',
  styleUrls: ['./music-edit.component.css']
})
export class MusicEditComponent implements OnInit {

  musicForm: FormGroup;
  id: string='';
  album_name: string='';
  artist_name: string='';
  genre: string='';
  label: string='';
  prize: string='';
  stock: string='';
  submitted = false;
  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router,private service:ServiceService) { }

  ngOnInit() {
    this.loadEdit();
    this.musicForm = this.formBuilder.group({
      album_name: ['', Validators.compose([Validators.required])],
      artist_name: ['', Validators.compose([Validators.required])],
      genre: ['', Validators.compose([Validators.required])],
      label: ['', Validators.compose([Validators.required])],
      prize: ['', Validators.compose([Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(4)])],
      stock: ['', Validators.compose([Validators.required, Validators.pattern("^[0-9]*$")])],
    });
  }

  loadEdit(){
    this.service.viewMusic(this.route.snapshot.params['id']).subscribe((data: any) => {
      this.id = data.id;
      this.musicForm.setValue({
        album_name: data.album_name,
        artist_name: data.artist_name,
        genre: data.genre,
        label: data.label,
        prize: data.prize,
        stock: data.stock
      });
    });

  }

  editMusic(){
    this.submitted = true;
    const values = {
      album_name: this.musicForm.controls.album_name.value,
      artist_name: this.musicForm.controls.artist_name.value,
      genre: this.musicForm.controls.genre.value,
      label: this.musicForm.controls.label.value,
      prize: this.musicForm.controls.prize.value,
      stock: this.musicForm.controls.stock.value,
    };
    if (this.musicForm.invalid) {
      return;
    }

    this.service.editMusic(this.id, values.album_name, values.artist_name, values.genre, values.label, values.prize, values.stock).subscribe(res => {
      // console.log(name.name);
        let id = res['id'];
        this.router.navigate(['/music']);
      }, (err) => {
        console.log(err);
      });

  }

}
